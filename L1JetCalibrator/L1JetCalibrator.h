//
// Author: Jona Bossio (jbossios@cern.ch)
// Date:   20 November 2019
//
///////////////////////////////////////////////////

#ifndef L1JetCalibrator_L1JetCalibrator_H
#define L1JetCalibrator_L1JetCalibrator_H

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/AnaToolHandle.h"

// ROOT
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TFile.h"

class L1JetCalibrator : public xAH::Algorithm
{
 public:
  // Algorithm constructor
  L1JetCalibrator ();

  /** @brief Name of the input L1 jet container */
  std::string m_inContainerName       = "";
  /** @brief Name of the output L1 jet container */
  std::string m_outContainerName      = "";
  /** @brief Name of the ROOT file containing the histogram of the L1 jet energy response vs Ereco and eta to be used to apply the MCJES */
  std::string m_L1MCJESROOTFileName   = "";
  /** @brief Name of the histogram of the L1 jet energy response vs Ereco and eta to be used to apply the MCJES */
  std::string m_L1JetResponseHistName = "";
  /** @brief Interpolate between bins (false by default) */
  bool        m_interpolate = false;
  /** @brief Apply correction based on pt instead of energy (default false) */
  bool        m_CorrVsPt = false;

  /** @brief TEvent object */
  xAOD::TEvent *m_event;  //!
  /** @brief TStore object for variable storage*/
  xAOD::TStore *m_store;  //!

  /** @brief Setup the job (inherits from Algorithm)*/
  virtual EL::StatusCode setupJob (EL::Job& job);
  /** @brief Execute the file (inherits from Algorithm)*/
  virtual EL::StatusCode fileExecute ();
  /** @brief Initialize the output histograms before any input file is attached (inherits from Algorithm)*/
  virtual EL::StatusCode histInitialize ();
  /** @brief Change to the next input file (inherits from Algorithm)*/
  virtual EL::StatusCode changeInput (bool firstFile);
  /** @brief Initialize the input file (inherits from Algorithm)*/
  virtual EL::StatusCode initialize ();
  /** @brief Execute each event of the input file (inherits from Algorithm)*/
  virtual EL::StatusCode execute ();
  /** @brief End the event execution (inherits from Algorithm)*/
  virtual EL::StatusCode postExecute ();
  /** @brief Finalize the input file (inherits from Algorithm)*/
  virtual EL::StatusCode finalize ();
  /** @brief Finalize the histograms after all files (inherits from Algorithm)*/
  virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
  /** @brief Configure the variables once before running*/
  virtual EL::StatusCode configure ();

  /** @brief Read jet energy response from histogram */
  double getJetEnergyResponseFromHist(double E, double eta);

private:
  // Configuration, and any other types of variables go here.

  /** @brief TH2 histogram with L1 jet response vs Ereco and eta */
  TH2D* m_L1JetResponseHist; //!

  double m_MeV2GeV = 0.001;
  double m_GeV2MeV = 1000.;

  ClassDef(L1JetCalibrator,1);

};

#endif
