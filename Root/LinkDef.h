#include <L1JetCalibrator/L1JetCalibrator.h>
#include <map>
#include <TString.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#endif

#ifdef __CINT__
#pragma link C++ class L1JetCalibrator+;

#endif
