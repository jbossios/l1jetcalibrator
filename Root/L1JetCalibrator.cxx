#include <AsgTools/MessageCheck.h>
#include <L1JetCalibrator/L1JetCalibrator.h>

// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>

// EDM include(s):
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODTrigger/JetRoIAuxContainer.h"
#include "PathResolver/PathResolver.h"
#include "AsgTools/MessageCheck.h"
#include "xAODCore/ShallowCopy.h"

// xAH
#include <xAODAnaHelpers/HelperFunctions.h>

using namespace std;
using namespace xAH;

ClassImp(L1JetCalibrator)

L1JetCalibrator :: L1JetCalibrator () :
  Algorithm("L1JetCalibrator")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}

EL::StatusCode  L1JetCalibrator :: configure (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode L1JetCalibrator :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "L1JetCalibrator" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode L1JetCalibrator :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_INFO( "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize())

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode L1JetCalibrator :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode L1JetCalibrator :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  (void)firstFile; //surpress unused param warning
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode L1JetCalibrator :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  // If there is no InputContainer we must stop
  if ( m_inContainerName.empty() ) {
    ANA_MSG_ERROR( "InputContainer is empty!");
    return EL::StatusCode::FAILURE;
  }
  // If there is no OutputContainer we must stop
  if ( m_outContainerName.empty() ) {
    ANA_MSG_ERROR( "OutputContainer is empty!");
    return EL::StatusCode::FAILURE;
  }

  // Retrieve TH2D histogram with L1 jet response
  ANA_MSG_INFO("A JES calibration will be applied to " << m_inContainerName);
  if(m_L1MCJESROOTFileName != "" && m_L1JetResponseHistName != ""){
    // Open TFile
    string fileName     = "L1JetCalibrator/"+m_L1MCJESROOTFileName;
    string fullFileName = PathResolverFindCalibFile(fileName.c_str());
    TFile* L1MCJESfile = TFile::Open(fullFileName.c_str());
    if(!L1MCJESfile){
      ANA_MSG_ERROR(m_L1MCJESROOTFileName << "not found, exiting");
      return EL::StatusCode::FAILURE;
    }
    // Get Response plot
    m_L1JetResponseHist = (TH2D*)L1MCJESfile->Get(m_L1JetResponseHistName.c_str());
    if(!m_L1JetResponseHist){
      ANA_MSG_ERROR(m_L1JetResponseHistName << "not found, exiting");
      return EL::StatusCode::FAILURE;
    }
    m_L1JetResponseHist->SetDirectory(0);
    L1MCJESfile->Close();
    delete L1MCJESfile;
  } else {
    ANA_MSG_ERROR("m_L1MCJESROOTFileName and/or m_L1JetResponseHistName are empty!, exiting");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode L1JetCalibrator :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
 
  // get the collection from TEvent or TStore
  ANA_MSG_DEBUG("Retrieving " << m_inContainerName << "jets");
  const xAOD::JetRoIContainer* inJets = 0;
  ANA_CHECK( HelperFunctions::retrieve(inJets, m_inContainerName, m_event, m_store, msg()) );
  ANA_MSG_DEBUG(m_inContainerName << "jets retrieved");

  // create shallow copy
  pair< xAOD::JetRoIContainer*, xAOD::ShallowAuxContainer* > calibJetsSC = xAOD::shallowCopyContainer( *inJets );
  string outSCContainerName    = m_outContainerName+"";
  string outSCAuxContainerName = m_outContainerName+"Aux.";
  ANA_CHECK( m_store->record( calibJetsSC.first,  outSCContainerName));
  ANA_CHECK( m_store->record( calibJetsSC.second, outSCAuxContainerName));

  // Loop over jets and apply correction
  for ( auto jet_itr : *(calibJetsSC.first) ) {
    TLorentzVector tlv;
    double et8x8 = jet_itr->et8x8()*m_MeV2GeV;
    double eta   = jet_itr->eta();
    double phi   = jet_itr->phi();
    tlv.SetPtEtaPhiM(et8x8,eta,phi,0);
    ANA_MSG_DEBUG("#########################################################");
    ANA_MSG_DEBUG("L1 jet et8x8 before etaJES calibration: "<< et8x8 << " GeV");
    ANA_MSG_DEBUG("L1 jet eta before etaJES calibration: "<< eta);
    double Corr = 1.;
    if(m_CorrVsPt){
      Corr = 1. / getJetEnergyResponseFromHist(tlv.Pt(),tlv.Eta());
    } else { // use energy (default)
      Corr = 1. / getJetEnergyResponseFromHist(tlv.E(),tlv.Eta());
    }
    tlv *= Corr;
    jet_itr->setEt8x8(tlv.Pt()*m_GeV2MeV);
    jet_itr->setEta(tlv.Eta());
    ANA_MSG_DEBUG("JES response: " << 1./Corr);
    ANA_MSG_DEBUG("JES correction factor: " << Corr);
    ANA_MSG_DEBUG("L1 jet et8x8 after JES calibration: "<< tlv.Pt() << " GeV");
    ANA_MSG_DEBUG("#########################################################");
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode L1JetCalibrator :: postExecute ()
{
  ANA_MSG_VERBOSE("postExecute()");
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode L1JetCalibrator :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode L1JetCalibrator :: histFinalize ()
{
  ANA_MSG_INFO("histFinalize()");
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  ANA_CHECK( xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}

double L1JetCalibrator :: getJetEnergyResponseFromHist(double E, double eta)
{
  int Ebin    = m_L1JetResponseHist->GetXaxis()->FindBin(E);
  int EMinbin = m_L1JetResponseHist->GetXaxis()->GetFirst();
  int EMaxbin = m_L1JetResponseHist->GetXaxis()->GetLast();
  int Etabin    = m_L1JetResponseHist->GetYaxis()->FindBin(eta);
  int EtaMinbin = m_L1JetResponseHist->GetYaxis()->GetFirst();
  int EtaMaxbin = m_L1JetResponseHist->GetYaxis()->GetLast();
  //Protection against input values that are outside the histogram range, which would cause TH2::Interpolate to throw an error
  if (Ebin < EMinbin) E = m_L1JetResponseHist->GetXaxis()->GetBinLowEdge(EMinbin)+1e-6;
  else if (Ebin > EMaxbin) E = m_L1JetResponseHist->GetXaxis()->GetBinUpEdge(EMaxbin)-1e-6;
  if (Etabin < EtaMinbin) eta = m_L1JetResponseHist->GetYaxis()->GetBinLowEdge(EtaMinbin)+1e-6;
  else if (Etabin > EtaMaxbin) eta = m_L1JetResponseHist->GetYaxis()->GetBinUpEdge(EtaMaxbin)-1e-6;
  //TH2::Interpolate is a bilinear interpolation from the bin centers.
  if(m_interpolate){return m_L1JetResponseHist->Interpolate(E, eta);}
  else{
    auto xbin = m_L1JetResponseHist->GetXaxis()->FindBin(E);
    auto ybin = m_L1JetResponseHist->GetYaxis()->FindBin(eta);
    return m_L1JetResponseHist->GetBinContent(xbin,ybin);
  }
}
